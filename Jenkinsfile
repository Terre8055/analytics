pipeline {
    agent any

    options {
        timestamps ()
        timeout (time: 10, unit: "MINUTES")
        gitLabConnection ("gitlab")
    }

    tools {
        maven "maven"
        jdk "openjdk-8"
    }

    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true, branchFilterType: 'All')
    }

    environment {
        REPO_URL = "git@gitlab.com:Terre8055/analytics.git"
    }

    stages {

        stage ("Checkout") {
            steps {
                checkout scm
            }
        }

        stage ("Set Version") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    sshagent(['image_pipe']){
                        sh"""
                            git fetch $REPO_URL --tags
                        """
                    }
                    env.VERSION=sh( returnStdout: true, script: "bash getTags.sh").trim()
                    println(env.VERSION)

                    sh"""
                        mvn versions:set -DnewVersion=$env.VERSION
                    """
                }
            }
            
        }


        stage ("Build -> Test") {
            when {
                branch "feature/*"
            }
            steps {
                script {
                    if ("${env.GIT_COMMIT_MESSAGE}".contains("#e2e")) {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package
                            """
                        }
                    } else {
                        withMaven(mavenSettingsConfig: "artifactory-settings") {
                            sh """
                                mvn package
                            """
                        }
                    }
                }
            }
        }

        stage ("Verify") {
            when {
                branch "main"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn verify
                    """
                    }
                }
            }
        }

        stage("E2E Testing") {
            when {
                    anyOf {
                        branch "main"
                        expression {
                            env.GIT_COMMIT_MESSAGE.contains("#e2e")
                        }
                    }
                }
            steps {
                script {
                    def version = "99.0.0-SNAPSHOT"
                    def componentJars = ["telemetry"]

                    def jars = componentJars.collect { component ->
                        'libs-snapshot-local/com/lidar/' + "$component/$version/${component}-${version}.jar"
                    }

                    echo "Constructed JAR paths:"
                    jars.each { jarPath ->
                        echo jarPath
                    }

                    def testsFile = version.contains("SNAPSHOT") ? "tests-sanity.txt" : "tests-full.txt"

                    sh "ls -al"

                    jars.each { jar ->
                        sh "curl -L -o ${jar.replace('/', '_')} http://artifactory:8081/artifactory/$jar"
                    }


                    echo "Running Simulator with JARs:"
                    jars.each { jar ->
                        echo jar
                    }
                    
                    echo "Test File: $testsFile"
                    sh "ls -al"
                    def javaClasspath = jars.collect { jar ->
                        jar.replaceAll('_', '/')
                    }.join(':')
                    sh "ls -al target/classes"
                    echo javaClasspath


                    
                    sh "java -cp ${javaClasspath}:target/classes com.lidar.simulation.Simulator"

                }
            }
        }


        stage ("Build -> Test -> Verify -> Publish") {
            when {
                branch "main"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Build -> Test -> Verify -> Publish -> Release") {
            when {
                branch "release/*"
            }
            steps {
                script {
                    withMaven(mavenSettingsConfig: "artifactory-settings"){
                    sh """
                        mvn dependency:list
                        mvn deploy -DskipTests
                    """
                    }
                }
            }
        }

        stage ("Tag") {
            when {
                branch "release/*"
            }
            steps{
                script{
                    sshagent(['image_pipe']){
                        sh"""
                            git clean -f -x
                            git tag $env.VERSION || echo "tag already exists"
                            git push --tags
                        """
                    }

                }
            }
        }
    }

    post {

        always {
            script {
                cleanWs()
            }
        }

        success {
            updateGitlabCommitStatus name: 'build', state: 'success'
        }

        failure {
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
    }
}
